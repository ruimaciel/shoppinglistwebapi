using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Application.Interfaces.Repositories;
using ShoppingList.Domain.Entities;
using ShoppingList.Persistence.Repositories;
using Xunit;

namespace ShoppingList.Persistence.Tests
{
    public class TestProductRepository : IDisposable
    {

        private readonly SqliteConnection _connection;
        private readonly ShoppingListDbContext _dbContext;
        private IProductRepository Repository { get; }

        public TestProductRepository()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();

            var options = new DbContextOptionsBuilder<ShoppingListDbContext>()
                .UseSqlite(_connection)
                .Options;

            // Create the schema in the database
            _dbContext = new ShoppingListDbContext(options);
            _dbContext.Database.EnsureCreated();

            Repository = new ProductRepository(_dbContext);
        }

        [Fact]
        public async Task Given_EmptyRepository_When_GetAll_Then_ReturnEmptyList()
        {
            //Given

            //When
            var entities = await Repository.GetAll();

            //Then
            Assert.True(entities.IsSuccess);
            Assert.Empty(entities.Success.Value);
        }

        [Fact]
        public async Task Given_EmptyRepository_When_FindByIdAnyProduct_Then_ResponseIsFailure()
        {
            //Given
            int someId = 0;

            //When
            var response = await Repository.FindById(someId, new CancellationToken());

            //Then
            Assert.True(response.IsFailure);
        }

        [Fact]
        public async Task Given_EmptyRepository_When_Add_Then_ResultIsSuccess()
        {
            //Given
            var entity = new Product
            {
                Name = "Foo"
            };

            //When
            var result = await Repository.Create(entity, new CancellationToken());

            //Then
            Assert.True(result.IsSuccess);
        }

        [Fact]
        public async Task Given_EmptyRepository_When_AddNewEntity_Then_ResultEntityNameIsEqualToEntityName()
        {
            //Given
            var inputEntity = new Product
            {
                Name = "Foo"
            };

            //When
            var result = await Repository.Create(inputEntity, new CancellationToken());

            //Then
            Assert.True(result.IsSuccess);
            Assert.Equal(inputEntity.Name, result.Success.Value.Name);
        }

        [Fact]
        public async Task Given_EmptyRepository_When_Delete_Then_ReturnFailure()
        {
            //Given
            var productId = 1;  // no record exists with this productId

            //When
            var result = await Repository.Delete(productId, new CancellationToken());

            //Then
            Assert.True(result.IsFailure);
            Assert.Equal(ErrorCode.NotFound, result.Failure.Value);
        }


        public void Dispose()
        {
            _connection.Close();
        }
    }
}
