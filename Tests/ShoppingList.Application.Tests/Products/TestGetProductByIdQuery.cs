using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Maciel.Monads;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using ShoppingList.Application.CQRS.Products.Queries.GetProductById;
using ShoppingList.Application.Interfaces.Repositories;
using ShoppingList.Domain.Entities;
using Xunit;
using ErrorCode = ShoppingList.Application.Interfaces.Repositories.ErrorCode;

namespace ShoppingList.Application.Tests
{
    public class TestGetProductByIdQuery
    {

        private IServiceProvider ServiceProvider { get; }

        public TestGetProductByIdQuery()
        {

            ServiceProvider = new ServiceCollection()
                .AddAutoMapper(typeof(Application.Infrastructure.AutoMapperProfile).GetTypeInfo().Assembly)
                .BuildServiceProvider();
        }

        [Fact]
        public async System.Threading.Tasks.Task Given_RepositoryWithProduct_When_GetProductById_Then_ReturnIsSuccess()
        {
            //Given
            var mockRepository = new Mock<IProductRepository>();

            Product entity = new Domain.Entities.Product
            {
                Id = 1,
                Name = "Foo"
            };

            mockRepository
                .Setup(repo => repo.FindById(entity.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new Success<Product>(entity));

            var command = new GetProductByIdQuery
            {
                ProductId = entity.Id
            };

            var handler = new GetProductByIdQueryHandler(mockRepository.Object, ServiceProvider.GetRequiredService<IMapper>());

            //When
            var response = await handler.Handle(command, new CancellationToken());

            //Then
            Assert.True(response.IsSuccess);
        }

        [Fact]
        public async System.Threading.Tasks.Task Given_RepositoryWithoutProduct_When_GetProductById_Then_ReturnIsSuccess()
        {
            //Given
            var mockRepository = new Mock<IProductRepository>();
            mockRepository
                .Setup(repo => repo.FindById(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new Failure<ErrorCode>(ErrorCode.NotFound));

            var command = new GetProductByIdQuery
            {
                ProductId = 0
            };

            var handler = new GetProductByIdQueryHandler(mockRepository.Object, ServiceProvider.GetRequiredService<IMapper>());

            //When
            var response = await handler.Handle(command, new CancellationToken());

            //Then
            Assert.True(response.IsFailure);
        }
    }
}
