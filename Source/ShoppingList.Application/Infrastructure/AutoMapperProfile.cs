// <copyright file="AutoMapperProfile.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.Infrastructure
{
    public class AutoMapperProfile : AutoMapper.Profile
    {
        public AutoMapperProfile()
        {
            this.CreateMap<Domain.Entities.Product, CQRS.Products.Queries.GetAllProducts.ProductDto>().ReverseMap();
            this.CreateMap<Domain.Entities.Product, CQRS.Products.Queries.GetProductById.ProductDto>().ReverseMap();

            this.CreateMap<Domain.Entities.Product, CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ProductDto>().ReverseMap();
            this.CreateMap<Domain.Entities.ShoppingTrip, CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ShoppingTripDto>().ReverseMap();
        }
    }
}
