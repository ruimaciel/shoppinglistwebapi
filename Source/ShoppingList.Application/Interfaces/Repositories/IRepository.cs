// <copyright file="IRepository.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.Interfaces.Repositories
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Maciel.Monads;

    public enum ErrorCode
    {
        None = 0,
        NotFound,
        UnknownError,
    }

    /*
    Inspired in:
    https://codebrains.io/asp-net-core-entity-framework-repository-pattern/
     */
    public interface IRepository<TEntity, TId>
        where TEntity : class
    {
        Task<Result<ErrorCode, TEntity>> Create(TEntity entity, CancellationToken token);

        Task<Result<ErrorCode, List<TEntity>>> GetAll(CancellationToken cancellationToken = default);

        Task<Result<ErrorCode, TEntity>> FindById(TId id, CancellationToken token);

        Task<Result<ErrorCode, TEntity>> Update(TEntity entity, CancellationToken token);

        Task<Result<ErrorCode, Nothing>> Delete(TId id, CancellationToken token);
    }
}
