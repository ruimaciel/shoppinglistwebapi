// <copyright file="IProductRepository.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.Interfaces.Repositories
{
    using ShoppingList.Domain.Entities;

    public interface IProductRepository : IRepository<Product, int>
    {
    }
}
