// <copyright file="GetAllProductsQueryHandler.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Queries.GetAllProducts
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Maciel.Monads;
    using MediatR;
    using ShoppingList.Application.Interfaces.Repositories;
    using OutputType = System.Collections.Generic.List<ShoppingList.Application.CQRS.Products.Queries.GetAllProducts.ProductDto>;
    using QueryResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Queries.GetAllProducts.ErrorCode, System.Collections.Generic.List<ShoppingList.Application.CQRS.Products.Queries.GetAllProducts.ProductDto>>;

    public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, QueryResult>
    {
        private readonly IProductRepository repository;
        private readonly IMapper mapper;

        public GetAllProductsQueryHandler(IProductRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<QueryResult> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            var result = await this.repository.GetAll(cancellationToken);

            if (result.IsFailure)
            {
                return QueryResult.MakeFailure(ErrorCode.Unknown);
            }

            var response = this.mapper.Map<OutputType>(result.Success.Value);

            return QueryResult.MakeSuccess(response);
        }
    }
}
