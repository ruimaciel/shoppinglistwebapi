// <copyright file="GetAllProductsQuery.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Queries.GetAllProducts
{
    using System.Collections.Generic;
    using Maciel.Monads;
    using MediatR;
    using QueryResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Queries.GetAllProducts.ErrorCode, System.Collections.Generic.List<ShoppingList.Application.CQRS.Products.Queries.GetAllProducts.ProductDto>>;

    public enum ErrorCode
    {
        Unknown,
    }

    public class GetAllProductsQuery : IRequest<QueryResult>
    {
    }
}
