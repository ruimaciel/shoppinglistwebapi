// <copyright file="ProductDto.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Queries.GetProductById
{
    public class ProductDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
