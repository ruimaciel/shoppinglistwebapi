// <copyright file="GetProductByIdQueryHandler.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Queries.GetProductById
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Maciel.Monads;
    using MediatR;
    using ShoppingList.Application.Interfaces.Repositories;
    using QueryResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Queries.GetProductById.ErrorCode, ShoppingList.Application.CQRS.Products.Queries.GetProductById.ProductDto>;

    public class GetProductByIdQueryHandler : IRequestHandler<GetProductByIdQuery, QueryResult>
    {
        private readonly IProductRepository productRepository;
        private readonly IMapper mapper;

        public GetProductByIdQueryHandler(IProductRepository productRepository, IMapper mapper)
        {
            this.productRepository = productRepository;
            this.mapper = mapper;
        }

        public async Task<QueryResult> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            var response = await this.productRepository.FindById(request.ProductId, cancellationToken);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case Interfaces.Repositories.ErrorCode.NotFound:
                        return QueryResult.MakeFailure(ErrorCode.NotFound);

                    default:
                        return QueryResult.MakeFailure(ErrorCode.Unknown);
                }
            }

            return QueryResult.MakeSuccess(this.mapper.Map<ProductDto>(response.Success.Value));
        }
    }
}
