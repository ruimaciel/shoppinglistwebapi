﻿// <copyright file="GetProductByIdQuery.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Queries.GetProductById
{
    using Maciel.Monads;
    using MediatR;
    using QueryResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Queries.GetProductById.ErrorCode, ShoppingList.Application.CQRS.Products.Queries.GetProductById.ProductDto>;

    public enum ErrorCode : byte
    {
        NotFound,
        Unknown,
    }

    public class GetProductByIdQuery : IRequest<QueryResult>
    {
        public int ProductId { get; set; }
    }
}
