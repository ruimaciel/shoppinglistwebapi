// <copyright file="DeleteProductByIdCommand.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Commands.DeleteProductById
{
    using Maciel.Monads;
    using MediatR;
    using CommandResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Commands.DeleteProductById.ErrorCode, Maciel.Monads.Nothing>;

    public enum ErrorCode
    {
        NotFound,
        Unknown,
    }

    public class DeleteProductByIdCommand : IRequest<CommandResult>
    {
        public int ProductId { get; set; }
    }
}
