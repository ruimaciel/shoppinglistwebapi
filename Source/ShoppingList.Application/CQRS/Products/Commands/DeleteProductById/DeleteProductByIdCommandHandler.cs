// <copyright file="DeleteProductByIdCommandHandler.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Commands.DeleteProductById
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Maciel.Monads;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using ShoppingList.Application.Interfaces.Repositories;
    using CommandResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Commands.DeleteProductById.ErrorCode, Maciel.Monads.Nothing>;

    public class DeleteProductByIdCommandHandler : IRequestHandler<DeleteProductByIdCommand, CommandResult>
    {
        private readonly IProductRepository repository;
        private readonly DbContext context;
        private readonly IMapper mapper;

        public DeleteProductByIdCommandHandler(IProductRepository repository, DbContext context, IMapper mapper)
        {
            this.repository = repository;
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<CommandResult> Handle(DeleteProductByIdCommand request, CancellationToken cancellationToken)
        {
            var response = await this.repository.Delete(request.ProductId, cancellationToken);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case Interfaces.Repositories.ErrorCode.NotFound:
                        return new Failure<ErrorCode>(ErrorCode.NotFound);

                    default:
                        return new Failure<ErrorCode>(ErrorCode.Unknown);
                }
            }

            return CommandResult.MakeSuccess(new Nothing());
        }
    }
}
