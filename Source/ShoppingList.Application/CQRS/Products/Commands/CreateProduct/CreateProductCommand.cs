// <copyright file="CreateProductCommand.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Commands.CreateProduct
{
    using Maciel.Monads;
    using MediatR;
    using CommandResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Commands.CreateProduct.ExitCode, int>;

    public enum ExitCode : byte
    {
        InvalidCommand,
        ProductAlreadyExists,
        Unknown,
    }

    public class CreateProductCommand : IRequest<CommandResult>
    {
        public string Name { get; set; }
    }
}
