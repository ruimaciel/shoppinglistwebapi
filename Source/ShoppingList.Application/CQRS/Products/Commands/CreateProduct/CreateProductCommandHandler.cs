// <copyright file="CreateProductCommandHandler.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Commands.CreateProduct
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Maciel.Monads;
    using MediatR;
    using ShoppingList.Application.Interfaces.Repositories;
    using CommandResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Commands.CreateProduct.ExitCode, int>;

    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, CommandResult>
    {
        private readonly IProductRepository repository;
        private readonly IMapper mapper;

        public CreateProductCommandHandler(IProductRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<CommandResult> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            // validate command
            var validationResult = new CreateProductCommandValidator().Validate(request);
            if (!validationResult.IsValid)
            {
                return CommandResult.MakeFailure(ExitCode.InvalidCommand);
            }

            // let's run the command
            var newEntity = new Domain.Entities.Product { Name = request.Name };

            var response = await this.repository.Create(newEntity, cancellationToken);

            if (response.IsFailure)
            {
                CommandResult.MakeFailure(ExitCode.Unknown);
            }

            return CommandResult.MakeSuccess(response.Success.Value.Id);
        }
    }
}
