// <copyright file="UpdateProductByIdCommandValidator.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Commands.UpdateProductById
{
    using FluentValidation;
    using ShoppingList.Domain.Validators;

    public class UpdateProductByIdCommandValidator : AbstractValidator<UpdateProductByIdCommand>
    {
        public UpdateProductByIdCommandValidator()
        {
            this.RuleFor(x => x.Name).IsValidProductName();
        }
    }
}
