// <copyright file="UpdateProductByIdCommand.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Commands.UpdateProductById
{
    using Maciel.Monads;
    using MediatR;
    using CommandResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Commands.UpdateProductById.ExitCode, int>;

    public enum ExitCode : byte
    {
        InvalidCommand,
        NotFound,
        Unknown,
    }

    public class UpdateProductByIdCommand : IRequest<CommandResult>
    {
        public int ProductId { get; set; }

        public string Name { get; set; }
    }
}
