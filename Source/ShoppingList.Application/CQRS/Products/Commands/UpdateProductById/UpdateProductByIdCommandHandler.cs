// <copyright file="UpdateProductByIdCommandHandler.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.Products.Commands.UpdateProductById
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Maciel.Monads;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using CommandResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.Products.Commands.UpdateProductById.ExitCode, int>;

    public class UpdateProductByIdCommandHandler : IRequestHandler<UpdateProductByIdCommand, CommandResult>
    {
        private readonly DbContext context;
        private readonly IMapper mapper;

        public UpdateProductByIdCommandHandler(DbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<CommandResult> Handle(UpdateProductByIdCommand request, CancellationToken cancellationToken)
        {
            // validate command
            var validationResult = new UpdateProductByIdCommandValidator().Validate(request);
            if (validationResult.IsValid)
            {
                return CommandResult.MakeFailure(ExitCode.InvalidCommand);
            }

            // dirty hack to avoid this bug in FindAsync():
            // https://github.com/aspnet/EntityFrameworkCore/issues/12012
            var entity = await this.context.Set<Domain.Entities.Product>().FindAsync(keyValues: new object[] { request.ProductId }, cancellationToken: cancellationToken);
            if (entity == null)
            {
                return CommandResult.MakeFailure(ExitCode.NotFound);
            }

            // update old entity
            entity.Name = request.Name;

            this.context.Entry<Domain.Entities.Product>(entity).State = EntityState.Modified;
            await this.context.SaveChangesAsync(cancellationToken);

            return CommandResult.MakeSuccess(entity.Id);
        }
    }
}
