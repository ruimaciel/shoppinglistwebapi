// <copyright file="GetAllShoppingTripsQuery.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips
{
    using System.Collections.Generic;
    using Maciel.Monads;
    using MediatR;
    using QueryResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ErrorCode, System.Collections.Generic.List<ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ShoppingTripDto>>;

    public enum ErrorCode
    {
        Unknown,
    }

    public class GetAllShoppingTripsQuery : IRequest<QueryResult>
    {
    }
}
