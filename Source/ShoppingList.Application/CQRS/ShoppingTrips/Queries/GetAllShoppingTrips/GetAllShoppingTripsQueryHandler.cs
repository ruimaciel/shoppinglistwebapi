// <copyright file="GetAllShoppingTripsQueryHandler.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Maciel.Monads;
    using MediatR;
    using ShoppingList.Application.Interfaces.Repositories;
    using OutputType = System.Collections.Generic.List<ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ShoppingTripDto>;
    using QueryResult = Maciel.Monads.Result<ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ErrorCode, System.Collections.Generic.List<ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ShoppingTripDto>>;

    public class GetAllShoppingTripsQueryHandler : IRequestHandler<GetAllShoppingTripsQuery, QueryResult>
    {
        private readonly IShoppingTripRepository repository;
        private readonly IMapper mapper;

        public GetAllShoppingTripsQueryHandler(IShoppingTripRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<QueryResult> Handle(GetAllShoppingTripsQuery request, CancellationToken cancellationToken)
        {
            var result = await this.repository.GetAll(cancellationToken);

            if (result.IsFailure)
            {
                return QueryResult.MakeFailure(ErrorCode.Unknown);
            }

            var shoppingTripEntityList = result.Success.Value;

            var shoppingTripList = this.mapper.Map<OutputType>(shoppingTripEntityList);

            return QueryResult.MakeSuccess(shoppingTripList);
        }
    }
}
