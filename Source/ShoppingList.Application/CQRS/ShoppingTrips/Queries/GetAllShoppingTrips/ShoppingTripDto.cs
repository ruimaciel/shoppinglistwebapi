// <copyright file="ShoppingTripDto.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips
{
    using System;
    using System.Collections.Generic;

    public class ShoppingTripDto
    {
        public DateTime Date { get; set; }

        public ICollection<ProductDto> Products { get; set; }
    }
}
