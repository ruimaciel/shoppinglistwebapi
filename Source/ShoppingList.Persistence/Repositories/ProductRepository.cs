// <copyright file="ProductRepository.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Persistence.Repositories
{
    using ShoppingList.Application.Interfaces.Repositories;
    using ShoppingList.Domain.Entities;

    public class ProductRepository : BaseRepository<Product, int>, IProductRepository
    {
        public ProductRepository(ShoppingListDbContext dbContext)
            : base(dbContext)
        {
        }
    }
}