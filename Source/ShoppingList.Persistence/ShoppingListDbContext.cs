﻿// <copyright file="ShoppingListDbContext.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Persistence
{
    using Microsoft.EntityFrameworkCore;
    using ShoppingList.Persistence.Configurations;

    public class ShoppingListDbContext : DbContext
    {
        public ShoppingListDbContext(DbContextOptions<ShoppingListDbContext> options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }

        public DbSet<Domain.Entities.Product> Products { get; set; }

        public DbSet<Domain.Entities.ShoppingTrip> ShoppingTrips { get; set; }

        public DbSet<Domain.Entities.ShoppingTripsProductsRelationship> ShoppingTripsProductsRelationship { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .ApplyConfiguration(new ProductConfiguration())
                .ApplyConfiguration(new ShoppingTripConfiguration())
                .ApplyConfiguration(new ShoppingTripsProductsRelationshipConfiguration());
        }
    }
}