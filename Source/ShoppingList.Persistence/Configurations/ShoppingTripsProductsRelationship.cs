// <copyright file="ShoppingTripsProductsRelationship.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Persistence.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using ShoppingList.Domain.Entities;

    public class ShoppingTripsProductsRelationshipConfiguration : IEntityTypeConfiguration<Domain.Entities.ShoppingTripsProductsRelationship>
    {
        public void Configure(EntityTypeBuilder<ShoppingTripsProductsRelationship> builder)
        {
            builder.HasKey(e => new { e.ProductId, e.ShoppingTripId });

            builder
                .HasOne(e => e.Product)
                .WithMany()
                .HasForeignKey(e => e.ProductId);

            builder
                .HasOne(e => e.ShoppingTrip)
                .WithMany()
                .HasForeignKey(e => e.ShoppingTripId);
        }
    }
}