// <copyright file="ShoppingTripConfiguration.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Persistence.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using ShoppingList.Domain.Entities;

    public class ShoppingTripConfiguration : IEntityTypeConfiguration<Domain.Entities.ShoppingTrip>
    {
        public void Configure(EntityTypeBuilder<ShoppingTrip> builder)
        {
            builder.HasKey(e => e.ShoppingTripId);

            builder.Property(e => e.ShoppingTripId)
                .ValueGeneratedOnAdd();
        }
    }
}
