// <copyright file="ServiceCollectionExtension.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Persistence
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using ShoppingList.Application.Interfaces.Repositories;
    using ShoppingList.Persistence.Repositories;

    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddPersistenceLayer(this IServiceCollection services, Action<DbContextOptionsBuilder> optionsAction)
        {
            services.AddDbContext<ShoppingListDbContext>(
                optionsAction: optionsAction);

            services.AddScoped<DbContext, ShoppingListDbContext>();

            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IShoppingTripRepository, ShoppingTripRepository>();

            return services;
        }
    }
}