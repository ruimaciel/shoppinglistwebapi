// <copyright file="ShoppingTripsController.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.WebUI.Areas.Api.Controllers
{
    using AutoMapper;
    using Halcyon.HAL;
    using MediatR;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips;

    [Authorize]
    [ApiController]
    [Area("api")]
    [Route("[controller]")]
    public class ShoppingTripsController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IMediator mediator;
        private readonly LinkGenerator linkgen;

        public ShoppingTripsController(IMapper mapper, IMediator mediator, LinkGenerator linkgen)
        {
            this.mapper = mapper;
            this.mediator = mediator;
            this.linkgen = linkgen;
        }

        [HttpGet("", Name = nameof(GetShoppingTripCollection))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetShoppingTripCollection()
        {
            var query = new GetAllShoppingTripsQuery();

            var result = await this.mediator.Send(query);

            if (result.IsFailure)
            {
                return this.NotFound();
            }

            var resource = result.Success.Value;

            // return Ok(value: responseResource);
            var halResponse = new HALResponse(null)
                .AddLinks(
                    new Link[]
                    {
                        new Link("self", this.Request.Path),
                        new Link("up", this.linkgen.GetPathByAction(this.HttpContext, action: nameof(RootController.Get), controller: "Root")),
                    })
                .AddEmbeddedCollection("shoppingtrips", resource);

            return this.Ok(halResponse);
        }
    }
}
