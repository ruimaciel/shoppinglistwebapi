// <copyright file="RootController.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.WebUI.Areas.Api.Controllers
{
    using Halcyon.HAL;
    using Microsoft.AspNetCore.Http.Extensions;
    using Microsoft.AspNetCore.Mvc;

    // [Authorize]
    [ApiController]
    [Area("api")]
    [Route("")]
    public class RootController : ControllerBase
    {
        protected readonly LinkGenerator linkgen;

        public RootController(LinkGenerator linken)
        {
            this.linkgen = linken;
        }

        [HttpGet]
        [Produces("application/hal+json")]
        public async Task<IActionResult> Get()
        {
            var response = new HALResponse(null)
                .AddLinks(
                    new Link[]
                    {
                        new Link("self", this.Request.Path),
                        new Link("curie", UriHelper.GetDisplayUrl(this.Request)) { Name = "sl" },
                        new Link("sl:products", this.linkgen.GetPathByAction(this.HttpContext, action: nameof(ProductsController.GetProductCollection), controller: "Products")),
                        new Link("sl:shoppingtrips", this.linkgen.GetPathByAction(this.HttpContext, action: nameof(ShoppingTripsController.GetShoppingTripCollection), controller: "ShoppingTrips")),
                    });

            return await Task.FromResult<IActionResult>(this.Ok(response));
        }
    }
}
