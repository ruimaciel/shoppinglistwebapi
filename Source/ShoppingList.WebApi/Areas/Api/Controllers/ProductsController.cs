﻿// <copyright file="ProductsController.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.WebUI.Areas.Api.Controllers
{
    using AutoMapper;
    using Halcyon.HAL;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using ShoppingList.Application.CQRS.Products.Commands.CreateProduct;
    using ShoppingList.Application.CQRS.Products.Commands.DeleteProductById;
    using ShoppingList.Application.CQRS.Products.Commands.UpdateProductById;
    using ShoppingList.Application.CQRS.Products.Queries.GetAllProducts;
    using ShoppingList.Application.CQRS.Products.Queries.GetProductById;
    using ShoppingList.WebUI.Areas.Api.Contract;
    using CQRS = ShoppingList.Application.CQRS;

    // [Authorize]
    [ApiController]
    [Area("api")]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IMediator mediator;

        public ProductsController(IMapper mapper, IMediator mediator)
        {
            this.mapper = mapper;
            this.mediator = mediator;
        }

        [HttpGet("", Name = nameof(GetProductCollection))]
        [ProducesResponseType(typeof(ProductCollectionResource), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetProductCollection()
        {
            var query = new GetAllProductsQuery();
            var response = await this.mediator.Send(query);

            if (response.IsFailure)
            {
                return this.NotFound();
            }

            var resource = this.mapper.Map<ProductCollectionResource>(response.Success.Value);

            // output JSON HAL
            var halResource = new HALResponse(resource)
                .AddLinks(
                    new Link[]
                    {
                        new Link("self", this.Request.Path),
                        new Link("product", "/Products/{productId}", replaceParameters: false),
                        new Link("up", "/"),
                    });

            return this.Ok(value: halResource);
        }

        [HttpGet("{productId:int}", Name = nameof(GetProductById))]
        [ProducesResponseType(typeof(ProductResource), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetProductById([FromRoute] int productId)
        {
            var query = new GetProductByIdQuery { ProductId = productId };
            var response = await this.mediator.Send(query);

            if (response.IsFailure)
            {
                return this.NotFound();
            }

            var resource = this.mapper.Map<ProductResource>(response.Success.Value);

            // assemble the HAL resource
            var halResource = new HALResponse(resource)
                .AddLinks(
                    new Link[]
                    {
                        new Link("self", this.Request.Path),
                        new Link("up", "/Products"),
                    });

            return this.Ok(value: halResource);
        }

        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateProduct([FromBody] ProductRequestDocument product)
        {
            var command = this.mapper.Map<CreateProductCommand>(product);
            var response = await this.mediator.Send(command);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case CQRS.Products.Commands.CreateProduct.ExitCode.ProductAlreadyExists:
                        return this.Conflict();

                    case CQRS.Products.Commands.CreateProduct.ExitCode.Unknown:
                    default:
                        return this.StatusCode(StatusCodes.Status500InternalServerError);
                }
            }

            return this.CreatedAtAction(actionName: nameof(this.GetProductById), routeValues: new { productId = response.Success.Value }, value: null);
        }

        [HttpPut("{productId:int}")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateProductById([FromRoute] int productId, [FromBody] ProductRequestDocument request)
        {
            var command = this.mapper.Map<UpdateProductByIdCommand>(request);
            command.ProductId = productId;
            var response = await this.mediator.Send(command);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case CQRS.Products.Commands.UpdateProductById.ExitCode.NotFound:
                        return this.NotFound();

                    case CQRS.Products.Commands.UpdateProductById.ExitCode.Unknown:
                    default:
                        return this.StatusCode(500);
                }
            }

            return this.Ok();
        }

        [HttpDelete("{productId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteProductById([FromRoute] int productId)
        {
            var command = new DeleteProductByIdCommand { ProductId = productId };

            var response = await this.mediator.Send(command);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case CQRS.Products.Commands.DeleteProductById.ErrorCode.NotFound:
                        return this.NotFound();

                    default:
                        return this.BadRequest();
                }
            }

            return this.NoContent();
        }
    }
}
