// <copyright file="MappingProfile.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.WebUI.Areas.Api.Contract
{
    using System.Collections.Generic;
    using AutoMapper;
    using CQRS = ShoppingList.Application.CQRS;

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            this.CreateMap<CQRS.Products.Queries.GetProductById.ProductDto, Contract.ProductItemResource>();
            this.CreateMap<CQRS.Products.Queries.GetProductById.ProductDto, Contract.ProductResource>();

            this.CreateMap<CQRS.Products.Queries.GetAllProducts.ProductDto, Contract.ProductResource>();
            this.CreateMap<CQRS.Products.Queries.GetAllProducts.ProductDto, Contract.ProductItemResource>();
            this.CreateMap<List<CQRS.Products.Queries.GetAllProducts.ProductDto>, Contract.ProductCollectionResource>()
                .ForMember(dest => dest.Products, opt => opt.MapFrom(src => src));

            this.CreateMap<Contract.ProductRequestDocument, CQRS.Products.Commands.CreateProduct.CreateProductCommand>();
            this.CreateMap<Contract.ProductRequestDocument, CQRS.Products.Commands.UpdateProductById.UpdateProductByIdCommand>();

            this.CreateMap<CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ShoppingTripDto, ShoppingTripResource>();
        }
    }
}
