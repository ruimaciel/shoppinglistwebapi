// <copyright file="ProductCollectionResource.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.WebUI.Areas.Api.Contract
{
    public class ProductCollectionResource
    {
        public List<ProductResource> Products { get; set; } = new List<ProductResource>();
    }
}