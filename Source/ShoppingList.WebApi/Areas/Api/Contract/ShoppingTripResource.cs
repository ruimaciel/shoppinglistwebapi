// <copyright file="ShoppingTripResource.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.WebUI.Areas.Api.Contract
{
    public class ShoppingTripResource
    {
        public DateTime DateTime { get; set; }

        public ICollection<ProductResource> Products { get; set; }
    }
}
