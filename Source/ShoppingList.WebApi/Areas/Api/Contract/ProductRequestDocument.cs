// <copyright file="ProductRequestDocument.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.WebUI.Areas.Api.Contract
{
    using FluentValidation;

    public class ProductRequestDocument
    {
        public string Name { get; set; }
    }

    public class ProductValidator : AbstractValidator<ProductRequestDocument>
    {
        public ProductValidator()
        {
            this.RuleFor(x => x.Name)
                .NotNull()
                .MinimumLength(2)
                .MaximumLength(64);
        }
    }
}
