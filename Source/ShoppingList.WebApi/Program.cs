// <copyright file="Program.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Reflection;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ShoppingList.Persistence;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(AppDomain.CurrentDomain.GetAssemblies()));
builder.Services.AddAutoMapper(
    typeof(ShoppingList.WebUI.Areas.Api.Contract.MappingProfile),
    typeof(ShoppingList.Application.Infrastructure.AutoMapperProfile));

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHealthChecks().AddDbContextCheck<ShoppingListDbContext>();

builder.Services.Configure<ShoppingList.WebUI.Options.Postgres>(builder.Configuration.GetSection("Postgres"));
var postgresOptions = builder.Services.BuildServiceProvider().GetService<IOptions<ShoppingList.WebUI.Options.Postgres>>().Value;
builder.Services.AddPersistenceLayer(
    optionsAction: options => options.UseNpgsql(postgresOptions.ConnectionString()));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

if (app.Environment.IsProduction())
{
    app.UseHttpsRedirection();
    app.UseAuthorization();
}

app.UseHealthChecks("/health", new Microsoft.AspNetCore.Diagnostics.HealthChecks.HealthCheckOptions()
{
    AllowCachingResponses = false,
});

app.MapControllers();

app.Run();
