// <copyright file="Postgres.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.WebUI.Options
{
    public class Postgres
    {
        public string Host { get; set; } = "localhost";

        public int Port { get; set; } = 5432;

        public string Database { get; set; } = "shoppinglist";

        public string Username { get; set; } = "postgres";

        public string Password { get; set; } = "postgres";

        public string ConnectionString()
        {
            // https://www.npgsql.org/doc/connection-string-parameters.html
            return $"Host={this.Host}; Port={this.Port}; Database={this.Database}; Username={this.Username}; Password={this.Password};";
        }
    }
}
