// <copyright file="ShoppingTrip.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class ShoppingTrip
    {
        public int ShoppingTripId { get; set; }

        public DateTime Date { get; set; }

        public ICollection<Product> Products { get; set; } = new List<Product>();
    }
}
