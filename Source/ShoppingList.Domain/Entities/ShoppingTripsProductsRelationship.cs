// <copyright file="ShoppingTripsProductsRelationship.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Domain.Entities
{
    public class ShoppingTripsProductsRelationship
    {
        public int ProductId { get; set; }

        public Product Product { get; set; }

        public int ShoppingTripId { get; set; }

        public ShoppingTrip ShoppingTrip { get; set; }
    }
}
