// <copyright file="ValidatorExtensions.cs" company="Rui Maciel">
// Copyright (c) Rui Maciel. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace ShoppingList.Domain.Validators
{
    using FluentValidation;

    /// <summary>
    /// Custom validator fo rFluentValidation.
    /// </summary>
    public static class ValidatorExtensions
    {
        /// <summary>
        /// High-level validation that performs all validations to determine if a string represents a valid
        /// product name.
        /// </summary>
        /// <remarks>
        /// Generic type is hard-coded to string, meaning this method will only appear for string properties.
        /// If you wanted it to appear for *all* properties, introduce a second type-parameter called TProperty.
        /// </remarks>
        /// <typeparam name="T">Type for which the validation applies.</typeparam>
        /// <param name="rule">FluentValidation's IRuleBuilder instance.</param>
        /// <returns>The same IRuleBuilder instance to continue being invoked in a fluent style.</returns>
        public static IRuleBuilderOptions<T, string> IsValidProductName<T>(this IRuleBuilder<T, string> rule)
        {
            return rule
                .NotNull()
                .MinimumLength(2)
                .MaximumLength(32);
        }
    }
}
